import os
import matplotlib.pyplot as plt
import math
from tqdm import tqdm

num_classes = 19
class_meaning = ['road', 'sidewalk', 'building', 'wall', 'fence',
                 'pole', 'traffic light', 'traffic sign', 'vegetation',
                 'terrain', 'sky', 'person', 'rider', 'car', 'truck',
                 'bus', 'train', 'motorcycle', 'bicycle']
classes = [i for i in range(num_classes)]
class_dictionary = dict()
directory = './results_resnet101'
val = ['frankfurt', 'lindau', 'munster']
classes_to_plot = ['road', 'vegetation', 'sky', 'car', 'building', 'traffic sign', 'person', 'bicycle', 'traffic light']
colors = ['#804080', '#6B8E23', '#4682B4', '#00008E', '#464646', '#DCDC00', '#DC143C', '#770B20', '#FAAA1E']
class_colors = dict()
result_dictionary = dict()
# deg_type = 'compress'
# compression_levels = ['100', '90', '70', '50', '30', '10']
# deg_type = 'blur'
# compression_levels = ['0', '3', '7', '11', '15', '19']
deg_type = 'noise'
compression_levels = ['0', '1000', '750', '500', '250', '100']


def do_class_mapping():
    for i in range(num_classes):
        class_dictionary[classes[i]] = class_meaning[i]


# check class performance per city
# check class performance per split (val)
def do_average_split(dictionary):
    class_average = dict()
    class_appearance = dict()
    for city in dictionary:
        for lvl in dictionary.get(city):
            for cls in dictionary.get(city).get(lvl):
                if cls not in class_average or cls not in class_appearance:
                    class_average[cls] = dict()
                    class_appearance[cls] = dict()
                if lvl not in class_average[cls] or lvl not in class_appearance[cls]:
                    class_average[cls][lvl] = 0
                    class_appearance[cls][lvl] = 0
                value = dictionary.get(city).get(lvl).get(cls)
                if not math.isnan(value):
                    class_average[cls][lvl] += value
                    class_appearance[cls][lvl] += 1
    for i in class_average:
        for lvl in class_average.get(i):
            class_average[i][lvl] /= class_appearance[i][lvl]
    return class_average


def do_average_city(dictionary):
    city_average = dict()
    city_appearance = dict()
    for city in dictionary:
        name = city.split('_')[0]
        if name in val:
            if name not in city_average or name not in city_appearance:
                city_average[name] = dict()
                city_appearance[name] = dict()
            for lvl in dictionary.get(city):
                for cls in dictionary.get(city).get(lvl):
                    if cls not in city_average[name] or cls not in city_appearance[name]:
                        city_average[name][cls] = dict()
                        city_appearance[name][cls] = dict()
                    if lvl not in city_average[name][cls] or lvl not in city_appearance[name][cls]:
                        city_average[name][cls][lvl] = 0
                        city_appearance[name][cls][lvl] = 0
                    value = dictionary.get(city).get(lvl).get(cls)
                    if not math.isnan(value):
                        city_average[name][cls][lvl] += value
                        city_appearance[name][cls][lvl] += 1
    for city in city_average:
        for cls in city_average.get(city):
            for lvl in city_average.get(city).get(cls):
                if city_appearance[city][cls][lvl] != 0:
                    city_average[city][cls][lvl] /= city_appearance[city][cls][lvl]
                else:
                    city_average[city][cls][lvl] = 0
    return city_average


def std_dev(results_dictionary, average_performance, cities):
    std_dev_dictionary = dict()  # {class : {intensity : value}}
    number_of_appearances = dict()  # {class : {intensity : # of appearances}}
    for i in classes:
        i = str(i)
        if i not in std_dev_dictionary or i not in number_of_appearances:
            std_dev_dictionary[i] = dict()
            number_of_appearances[i] = dict()
        for city in results_dictionary:
            city_name = city.split('_')[0]
            if city_name in cities:
                for lvl in results_dictionary.get(city):
                    if lvl not in std_dev_dictionary[i] or lvl not in number_of_appearances[i]:
                        std_dev_dictionary[i][lvl] = 0
                        number_of_appearances[i][lvl] = 0
                    value = results_dictionary.get(city).get(lvl).get(i)
                    if not math.isnan(value):
                        squared_difference = (value - average_performance.get(i).get(lvl)) ** 2
                        std_dev_dictionary[i][lvl] += squared_difference
                        number_of_appearances[i][lvl] += 1
    for i in std_dev_dictionary:
        for j in std_dev_dictionary.get(i):
            std_dev_dictionary[i][j] /= number_of_appearances.get(i).get(j)
            std_dev_dictionary[i][j] = math.sqrt(std_dev_dictionary[i][j])
    return std_dev_dictionary


def std_dev_city(results_dictionary, average_performance, cities):
    std_dev_dictionary = dict()  # {class : {intensity : value}}
    number_of_appearances = dict()  # {class : {intensity : # of appearances}}
    for i in classes:
        i = str(i)
        if i not in std_dev_dictionary or i not in number_of_appearances:
            std_dev_dictionary[i] = dict()
            number_of_appearances[i] = dict()
        for city in results_dictionary:
            city_name = city.split('_')[0]
            if city_name in cities:
                for lvl in results_dictionary.get(city):
                    if lvl not in std_dev_dictionary[i] or lvl not in number_of_appearances[i]:
                        std_dev_dictionary[i][lvl] = 0
                        number_of_appearances[i][lvl] = 0
                    value = results_dictionary.get(city).get(lvl).get(i)
                    if not math.isnan(value):
                        squared_difference = (value - average_performance.get(city_name).get(i).get(lvl)) ** 2
                        std_dev_dictionary[i][lvl] += squared_difference
                        number_of_appearances[i][lvl] += 1
    for i in std_dev_dictionary:
        for j in std_dev_dictionary.get(i):
            if number_of_appearances.get(i).get(j) != 0:
                std_dev_dictionary[i][j] /= number_of_appearances.get(i).get(j)
                std_dev_dictionary[i][j] = math.sqrt(std_dev_dictionary[i][j])
            else:
                std_dev_dictionary[i][j] = 0
    return std_dev_dictionary


def asymmetric_error(y_values, err_values):
    lower_errors = []
    upper_errors = []
    for i in range(len(y_values)):
        if y_values[i] + err_values[i] > 1.0:
            lower_errors.append(100*err_values[i])
            upper_errors.append(100*(1.0 - y_values[i]))
        elif y_values[i] - err_values[i] < 0.0:
            lower_errors.append(100*y_values[i])
            upper_errors.append(100*err_values[i])
        else:
            lower_errors.append(100*err_values[i])
            upper_errors.append(100*err_values[i])
    return [lower_errors, upper_errors]


def do_plot(average_performance, err, label):
    for cls in average_performance:
        x_values = []
        y_values = []
        err_values = []
        for i in compression_levels:
            if (i == '100' and deg_type == 'compress') or (i == '0' and (deg_type == 'blur' or deg_type == 'noise')):
                x_values.append('Normal image')
            else:
                x_values.append(i)
            y_values.append(average_performance.get(cls).get(deg_type+'_'+i))
            err_values.append(err.get(cls).get(deg_type+'_'+i))
        asym_errors = asymmetric_error(y_values, err_values)
        y_percentage = [k*100 for k in y_values]
        err_percentage = [k*100 for k in err_values]
        plt.errorbar(x_values, y_percentage, yerr=asym_errors, linestyle='dashed', marker='o', capsize=3)
        addtext_v3(x_values, y_percentage, y_percentage, asym_errors)
        plt.ylabel('Mean IoU%')
        if deg_type == 'compress':
            plt.xlabel('JPEG compression quality')
        elif deg_type == 'blur':
            plt.xlabel('Motion blur filter size')
        elif deg_type == 'noise':
            plt.xlabel('Variation in number of photons per pixel')
        plt.title('Average performance of '+class_dictionary.get(int(cls))+' class in '+label+' split with standard deviation', wrap=True)
        plt.savefig('./val_avg_'+class_dictionary.get(int(cls))+'_performance.png')
        plt.show()


def do_plot_city(average_performance, err, label):
    for cls in average_performance:
        x_values = []
        y_values = []
        err_values = []
        for i in compression_levels:
            if (i == '100' and deg_type == 'compress') or (i == '0' and (deg_type == 'blur' or deg_type == 'noise')):
                x_values.append('Normal image')
            else:
                x_values.append(i)
            y_values.append(average_performance.get(cls).get(deg_type+'_'+i))
            err_values.append(err.get(cls).get(deg_type+'_'+i))
        asym_errors = asymmetric_error(y_values, err_values)
        y_percentage = [k*100 for k in y_values]
        err_percentage = [k*100 for k in err_values]
        plt.errorbar(x_values, y_percentage, yerr=asym_errors, linestyle='dashed', marker='o', capsize=3)
        addtext_v3(x_values, y_percentage, y_percentage, asym_errors)
        plt.ylabel('Mean IoU%')
        if deg_type == 'compress':
            plt.xlabel('JPEG compression quality')
        elif deg_type == 'blur':
            plt.xlabel('Motion blur filter size')
        elif deg_type == 'noise':
            plt.xlabel('Variation in number of photons per pixel')
        plt.title('Average performance of '+class_dictionary.get(int(cls))+' class in '+label+' split with standard deviation', wrap=True)
        plt.savefig('./'+label+'_avg_'+class_dictionary.get(int(cls))+'_performance.png')
        plt.show()


def group_plot(cls_average_val_split):
    for i in cls_average_val_split:
        if class_dictionary.get(int(i)) in classes_to_plot:
            x_values = []
            y_values = []
            for lvl in compression_levels:
                if (lvl == '100' and deg_type == 'compress') or (lvl == '0' and (deg_type == 'blur' or deg_type == 'noise')):
                    x_values.append('Normal image')
                else:
                    x_values.append(lvl)
                y_values.append(cls_average_val_split.get(i).get(deg_type+'_'+lvl))
            y_percentage = [k*100 for k in y_values]
            plt.plot(x_values, y_percentage, label=class_dictionary.get(int(i)), c=class_colors.get(class_dictionary.get(int(i))))

    plt.ylabel('Mean IoU%')
    if deg_type == 'compress':
        plt.xlabel('JPEG compression quality')
    elif deg_type == 'blur':
        plt.xlabel('Motion blur filter size')
    elif deg_type == 'noise':
        plt.xlabel('Variation in number of photons per pixel')
    plt.title('Average performance of all classes in evaluation split')
    plt.savefig('./grouped_classes.png', bbox_extra_artists=(plt.legend(bbox_to_anchor=(1, 1), loc="upper left"),), bbox_inches='tight')
    plt.show()


def addtext_v3(x, y, text, err):
    for i in range(len(x)):
        # if round(err[1][i], 2) == round(err[0][i], 2):
        #     plt.text(x[i], y[i], str(round(text[i], 2))+' ± '+str(round(err[1][i], 2)))
        # else:
        plt.text(x[i], y[i] + err[1][i], '+'+str(round(err[1][i], 2)))
        plt.text(x[i], y[i] - err[0][i], '-' + str(round(err[0][i], 2)))
        plt.text(x[i], y[i], str(round(text[i], 2)))


def do_class_color_mapping():
    for i in range(len(classes_to_plot)):
        class_colors[classes_to_plot[i]] = colors[i]


# 2 cases - ignore class with nan value - only this is done
#         - treat it as correctly classified -> iou = 1.0
if __name__ == "__main__":
    do_class_mapping()
    do_class_color_mapping()
    ls = os.listdir(directory)
    for i in tqdm(ls):
        city_name = i.split('_')[0]
        if city_name in val:
            if i not in result_dictionary:
                result_dictionary[i] = dict()
            for j in os.listdir(os.path.join(directory, i)):
                if deg_type in j:
                    if j not in result_dictionary[i]:
                        result_dictionary[i][j] = dict()
                    files = os.listdir(os.path.join(directory, i, j))
                    for file in files:
                        if len(file.split('.')) < 2:
                            reader = open(os.path.join(directory, i, j, file), 'r')
                            lines = reader.readlines()
                            for line in lines:
                                if line.__contains__('class'):
                                    spl = line.split(':')
                                    class_number = spl[0].split(' ')[1]
                                    if class_number not in result_dictionary[i][j]:
                                        result_dictionary[i][j][class_number] = 0
                                    result_dictionary[i][j][class_number] = float(spl[1])
                            break
    class_average_for_val_split = do_average_split(result_dictionary)
    group_plot(class_average_for_val_split)
    std_dev_val = std_dev(result_dictionary, class_average_for_val_split, val)
    do_plot(class_average_for_val_split, std_dev_val, 'evaluation')
    class_average_for_city = do_average_city(result_dictionary)
    for i in val:
        std_dev_city_dictionary = std_dev_city(result_dictionary, class_average_for_city, [i])
        do_plot_city(class_average_for_city.get(i), std_dev_city_dictionary, i)


