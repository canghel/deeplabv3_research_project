# DeepLabV3+ Research Project

This repository contains files that were used to simulate 3 types of degradations, and then use these degraded images to evaluate model performance.

The types of degradation simulated are compression, motion blur and shot noise.\
DeepLabV3+ is used with weights obtained from MobileNet and ResNet-101 backbones.

The dataset used is Cityscapes, which can be downloaded from this link https://www.cityscapes-dataset.com/
