import os

import matplotlib.pyplot as plt
val = ['frankfurt', 'lindau', 'munster']
dir = './results_resnet101'
deg_type = 'compress'
if __name__ == "__main__":
    split_directories = ['test', 'train', 'val']
    cities2directories = dict()
    for i in split_directories:
        cities2directories[i] = []

    images_path = './datasets/data/cityscapes/leftImg8bit'

    all_cities = []
    compression_intensities = ['10', '30', '50', '70', '90', '100']
    #  {city -> {compression_quality -> (sum for all splits of city, #instances)}}
    for split in os.listdir(images_path):
        current_list = cities2directories.get(split)
        for city in os.listdir(os.path.join(images_path, split)):
            current_list.append(city)
            all_cities.append(city)
        cities2directories[split] = current_list

    result_dict = dict()
    ls = os.listdir(dir)
    for i in ls:  # loop through results directory
        degs = os.listdir(os.path.join(dir, i))
        city_name = i.split('_')[0]  # get city name
        if city_name in val:
            if not result_dict.__contains__(city_name):  # if dictionary does not contain city, initialize it
                result_dict[city_name] = dict()
            for j in degs:  # loop through city split sub-folders
                if j.__contains__(deg_type):  # found deg type we are looking for
                    compression_quality = j.split('_')[1]  # get intensity
                    if not result_dict[city_name].__contains__(compression_quality):  # dictionary does not contain tuple of values for compression quality for a city
                        result_dict[city_name][compression_quality] = (0, 0)
                    files = os.listdir(os.path.join(dir, i, j))
                    for file in files:  # loop through individual files
                        if file.startswith('0_image'):
                            size = os.path.getsize(os.path.join(dir, i, j, file))
                            tpl = result_dict[city_name][compression_quality]
                            a1 = list(tpl)
                            a1[0] += size
                            a1[1] += 1
                            result_dict[city_name][compression_quality] = (a1[0], a1[1])
                            break

    new_dict = dict()
    for i in result_dict:
        for j in result_dict.get(i):
            k = result_dict.get(i).get(j)
            k1 = list(k)
            final = 0
            if k1[1] != 0:
                final = k1[0] / k1[1]
            if not new_dict.__contains__(i):
                new_dict[i] = dict()
            if not new_dict[i].__contains__(j):
                new_dict[i][j] = 0
            new_dict[i][j] = final

    avg_sizes = []
    for i in compression_intensities:
        avg_size = 0
        instances = 0
        for city in new_dict:
            avg_size += new_dict.get(city).get(i)
            instances += 1
        avg_size /= instances
        avg_sizes.append(avg_size)
    avg_size_reduction = [avg_sizes[len(avg_sizes) - 1] - i for i in avg_sizes]
    percentage_reduction = [round((i*100) / avg_sizes[len(avg_sizes) - 1], 2) for i in avg_size_reduction]
    print(percentage_reduction)

