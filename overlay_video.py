import argparse
import os.path
import time
from torchvision import transforms as T
import network
import numpy as np
import cv2
import torch
import torch.nn as nn
from datasets import Cityscapes

metrics = None

def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, default="./test_results")
    parser.add_argument("--video", type=str, default=None)
    parser.add_argument("--image_directory", type=str, default="./datasets/data")
    parser.add_argument("--save_to", type=str, default="./overlay")
    parser.add_argument("--model", type=str, default="deeplabv3plus_resnet101")
    parser.add_argument("--ckpt", default=None, type=str,
                        help="resume from checkpoint")
    parser.add_argument("--gpu_id", type=str, default='0',
                        help="GPU ID")
    parser.add_argument("--crop_val", action='store_true', default=False,
                        help='crop validation (default: False)')
    return parser


def image_overlay(image, segmented_image):
    alpha = 0.7 # transparency for the original image
    beta = 1 # transparency for the segmentation map
    gamma = 0 # scalar added to each sum
    segmented_image = cv2.cvtColor(segmented_image, cv2.IMREAD_COLOR)
    image = np.array(image)
    image = cv2.cvtColor(image, cv2.IMREAD_COLOR)
    cv2.addWeighted(image, alpha, segmented_image, beta, gamma, image)
    return image


def segment_video(opts):
    capture = cv2.VideoCapture(opts.video)
    if not capture.isOpened():
        print("Error while trying to read video.")
    frame_width = int(capture.get(3))
    frame_height = int(capture.get(4))

    save_name = f"{opts.video.split('/')[-1].split('.')}"

    out = cv2.VideoWriter(f"./video_output/{save_name}.mp4",
                          cv2.VideoWriter_fourcc(*'mp4v'), 30,
                          (frame_width, frame_height))

    frame_count = 0
    total_fps = 0

    decode_fn = Cityscapes.decode_target

    os.environ['CUDA_VISIBLE_DEVICES'] = opts.gpu_id
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = network.modeling.__dict__[opts.model](num_classes=19, output_stride=16)

    checkpoint = torch.load(opts.ckpt, map_location=torch.device('cpu'))
    model.load_state_dict(checkpoint["model_state"])
    model = nn.DataParallel(model)
    model.to(device)

    if opts.crop_val:
        transform = T.Compose([
                T.Resize(opts.crop_size),
                T.CenterCrop(opts.crop_size),
                T.ToTensor(),
                T.Normalize(mean=[0.485, 0.456, 0.406],
                                std=[0.229, 0.224, 0.225]),
            ])
    else:
        transform = T.Compose([
                T.ToTensor(),
                T.Normalize(mean=[0.485, 0.456, 0.406],
                                std=[0.229, 0.224, 0.225]),
            ])

    while capture.isOpened():
        ret, frame = capture.read()
        if ret:
            rgb_frame = cv2.cvtColor(frame, cv2.IMREAD_COLOR)
            start_time = time.time()
            if frame_count < 6000:
                frame_count += 1
                continue
            # maybe only use frame for prediction, not RGB frame
            with torch.no_grad():
                model = model.eval()
                img = transform(frame).unsqueeze(0)
                img = img.to(device)

                frame_prediction = model(img).max(1)[1].cpu().numpy()[0]

                colorized_frame_prediction = decode_fn(frame_prediction).astype('uint8')
                # colorized_frame_prediction = Image.fromarray(colorized_frame_prediction)

                overlayed_frame = image_overlay(rgb_frame, colorized_frame_prediction)

            end_time = time.time()

            fps = 1 / (end_time-start_time)
            total_fps += fps
            frame_count += 1
            # print("frame #"+str(frame_count))

            # if frame_count == 10000:
            #     break

            cv2.putText(overlayed_frame, f"{fps:.3f} FPS", (20, 35),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
            # press `q` to exit
            # cv2.imshow('image', overlayed_frame)
            out.write(overlayed_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            # if frame_count > 6500:
            #     break
        else:
            print("Error reading frame, return value is: "+ret)
            break
    capture.release()
    cv2.destroyAllWindows()
    avg_fps = total_fps / frame_count
    print(f"Average FPS: {avg_fps:.3f}")


def main():
    opts = get_arguments().parse_args()

    if opts.video is not None:
        segment_video(opts)
        return


if __name__ == "__main__":
    main()
