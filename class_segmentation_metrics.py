import math
import os

directory = './results_resnet101'
deg_type = 'compress'
results_dict = dict()
filename = 'class_segmentation_metrics_'+deg_type+'.txt'


if __name__ == "__main__":
    #  (city : (deg_type : (class# : (avg, #instances)))
    ls = os.listdir(directory)
    for i in ls:
        city_name = i.split('_')[0]
        if not results_dict.__contains__(city_name):
            results_dict[city_name] = dict()
        for j in os.listdir(os.path.join(directory, i)):
            if j.__contains__(deg_type):
                compress_quality = j.split('_')[1]
                if not results_dict[city_name].__contains__(compress_quality):
                    results_dict[city_name][compress_quality] = dict()
                files = os.listdir(os.path.join(directory, i, j))
                for file in files:
                    if len(file.split('.')) < 2:
                        reader = open(os.path.join(directory, i, j, file), 'r')
                        lines = reader.readlines()
                        for line in lines:
                            if line.__contains__('class'):
                                spl = line.split(':')
                                class_number = spl[0].split(' ')[1]
                                if not results_dict[city_name][compress_quality].__contains__(class_number):
                                    results_dict[city_name][compress_quality][class_number] = (0, 0)
                                value = None
                                if not math.isnan(float(spl[1])):
                                    value = float(spl[1])
                                tpl = results_dict[city_name][compress_quality][class_number]
                                a1 = list(tpl)
                                if value is not None:
                                    a1[0] += value
                                    a1[1] += 1
                                results_dict[city_name][compress_quality][class_number] = (a1[0], a1[1])
    print(results_dict)
    for i in results_dict:
        for j in results_dict.get(i):
            for cls in results_dict.get(i).get(j):
                tpl = results_dict.get(i).get(j).get(cls)
                a1 = list(tpl)
                v = 0
                if a1[1] != 0:
                    v = a1[0]/a1[1]
                results_dict[i][j][cls] = v
    print(results_dict)
    with open('./'+filename, 'w+') as f:
        f.write(str(results_dict))


