import os.path
import random
import cv2
from PIL import Image, ImageFilter
import argparse
import numpy as np

PATH = "./blurry"  # this is where the blurred images will be saved to


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_directory", type=str, default=None)
    parser.add_argument("--output_directory", type=str, default="")
    parser.add_argument("--input_file", type=str, default=None)
    parser.add_argument("--blur_strength", type=int, default=1)
    parser.add_argument("--levels", type=int, default=1)
    parser.add_argument("--step", type=int, default=1)
    parser.add_argument("--entire_dataset", action="store_true", default=False)
    return parser


def main():
        opts = get_arguments().parse_args()
        if opts.input_file is not None:
            if os.path.isfile(opts.input_file):
                splt = opts.input_file.split('/')
                for i in range(opts.levels):
                    blur(opts, Image.open(opts.input_file), splt[len(splt) - 1])
                    opts.blur_strength += 1
                return
        steps = [0, 3, 7, 11, 15, 19]
        if opts.entire_dataset:
            initial_path = './datasets/data/cityscapes/leftImg8bit'
        leftImg8bit = os.listdir(initial_path)
        if not os.path.exists(PATH):
            os.mkdir(PATH)
        for i in steps:
            print(f"doing strength: {i}")
            path_1 = PATH+"/blurred_images_"+str(i)
            if not os.path.exists(path_1):
                os.mkdir(path_1)
            for split in leftImg8bit:
                if split != "test" and split != "train":
                    path_2 = path_1 + '/' + split
                    if not os.path.exists(path_2):
                        os.mkdir(path_2)
                    split_directory = os.listdir(initial_path+'/'+split)
                    for city in split_directory:

                        print(f"doing city: {city}")
                        path_3 = path_2 + '/' + city
                        if not os.path.exists(path_3):
                            os.mkdir(path_3)
                        city_directory = os.listdir(initial_path+'/'+split+'/'+city)
                        for file in city_directory:
                            path_4 = path_3 + '/' + file
                            if i > 0:
                                try:
                                    img = cv2.imread('./datasets/data/cityscapes/leftImg8bit/'+split+'/'+city+'/'+file)
                                    motion_blur(img, i, path_4)
                                except Exception:
                                    print("error while reading image ./datasets/data/cityscapes/leftImg8bit/"+split+"/"+city+"/"+file)
                            else:
                                try:
                                    cv2.imwrite(path_4, cv2.imread('./datasets/data/cityscapes/leftImg8bit/'+split+'/'+city+'/'+file))
                                except Exception:
                                    print("error while writing image")


def motion_blur(img, strength, path_to_save):
    # The greater the size, the more the motion.
    kernel_size = strength

    # Create the vertical kernel.
    kernel_v = np.zeros((kernel_size, kernel_size))

    # Create a copy of the same for creating the horizontal kernel.
    kernel_h = np.copy(kernel_v)

    # Fill the middle row with ones.
    kernel_v[:, int((kernel_size - 1) / 2)] = np.ones(kernel_size)
    kernel_h[int((kernel_size - 1) / 2), :] = np.ones(kernel_size)

    # Normalize.
    kernel_v /= kernel_size
    kernel_h /= kernel_size

    # Apply the vertical kernel.
    vertical_mb = cv2.filter2D(img, -1, kernel_v)

    # Apply the horizontal kernel.
    horizonal_mb = cv2.filter2D(img, -1, kernel_h)

    # 50% chance to either use vertical or horizontal motion blur
    try:
        if random.randint(1, 10) <= 5:
            cv2.imwrite(path_to_save, vertical_mb)
        else:
            cv2.imwrite(path_to_save, horizonal_mb)
    except Exception:
        print("caught exception while writing image")


def blur(opts, image, filename):
    blurred_image = image.filter(ImageFilter.GaussianBlur(opts.blur_strength))
    if not os.path.exists(PATH):
        os.mkdir(PATH)
    if not os.path.exists('./'+PATH+'/blurred_images_' + str(opts.blur_strength)):
        os.mkdir(PATH+'/blurred_images_' + str(opts.blur_strength))
    if filename is not None:
        blurred_image.save(PATH+'/blurred_images_' + str(opts.blur_strength) + '/' + filename)



if __name__ == "__main__":
    main()
