import math
import os

import matplotlib.pyplot as plt

directory = './results_resnet101'
val = ['frankfurt', 'lindau', 'munster']
train = ['aachen', 'bochum', 'bremen', 'cologne', 'darmstadt', 'dusseldorf', 'erfurt', 'hamburg', 'hanover', 'jena', 'krefeld', 'monchengladbach', 'strasbourg', 'stuttgart', 'tubingen', 'ulm', 'weimar', 'zurich']
results_dictionary = dict()
deg_type = 'compress'
compression_levels = ['100', '90', '70', '50', '30', '10']
# deg_type = 'blur'
# compression_levels = ['0', '3', '7', '11', '15', '19']
# deg_type = 'noise'
# compression_levels = ['0', '1000', '750', '500', '250', '100']


#  {city -> {deg_type -> mIoU}}
def average_per_city(result_dictionary):
    cities = dict()
    city_occurrence_count = dict()
    for i in result_dictionary:
        name = i.split('_')[0]
        if name not in city_occurrence_count:
            city_occurrence_count[name] = 0
        city_occurrence_count[name] += 1

    for entry in result_dictionary:
        entry_name = entry.split('_')[0]
        if entry_name not in cities:
            cities[entry_name] = dict()
        for degradation in result_dictionary.get(entry):
            if degradation not in cities[entry_name]:
                cities[entry_name][degradation] = 0
            cities[entry_name][degradation] += result_dictionary.get(entry).get(degradation)

    total_images = 0
    for i in city_occurrence_count:
        total_images += city_occurrence_count.get(i)
    # avg_val_split = average_per_split(cities, total_images)

    for entry in cities:
        for deg in cities.get(entry):
            cities[entry][deg] /= city_occurrence_count.get(entry)

    return cities #, avg_val_split


def average_per_split(dictionary, total_images):
    return_dictionary = dict()
    for i in dictionary:
        for j in dictionary.get(i):
            if j not in return_dictionary:
                return_dictionary[j] = 0
            return_dictionary[j] += dictionary.get(i).get(j)
    for i in return_dictionary:
        return_dictionary[i] /= total_images
    return return_dictionary


def addtext_v2(x, y, text, std_dev):
    for i in range(len(x)):
        plt.text(x[i], y[i], str(text[i]) + ' ± '+str(round(std_dev[i], 2)))


def plot_cities(x, y, err, title):
    plt.title('Average performance of '+title+' city split with standard deviation')
    if deg_type == 'compress':
        plt.xlabel('JPEG compression quality')
    elif deg_type == 'blur':
        plt.xlabel('Motion blur filter size')
    elif deg_type == 'noise':
        plt.xlabel('Variation in number of photons per pixel')
    plt.ylabel('Mean IoU%')
    y_percentage = [round(l*100, 2) for l in y]
    err_percentage = [100*k for k in err]
    addtext_v2(x, y_percentage, y_percentage, err_percentage)
    plt.errorbar(x, y_percentage, err_percentage, linestyle='dashed', marker='o', capsize=3)
    if not os.path.exists('./plots/'+title):
        os.mkdir('./plots/'+title)
        if not os.path.exists('./plots/' + title+'/'+deg_type):
            os.mkdir('./plots/' + title+'/'+deg_type)
    plt.savefig('./plots/'+title+'/'+deg_type+'/avg_performance_with_std_dev_munster.png')
    plt.show()


def plot_val_split(x, y, err):
    plt.title('Average performance of evaluation split with standard deviation')
    if deg_type == 'compress':
        plt.xlabel('JPEG compression quality')
    elif deg_type == 'blur':
        plt.xlabel('Motion blur filter size')
    elif deg_type == 'noise':
        plt.xlabel('Variation in number of photons per pixel')
    plt.ylabel('Mean IoU%')
    y_percentage = [round(l*100, 2) for l in y]
    err_percentage = [100*k for k in err]
    addtext_v2(x, y_percentage, y_percentage, err_percentage)
    plt.errorbar(x, y_percentage, err_percentage, linestyle='dashed', marker='o', capsize=3)
    if not os.path.exists('./plots/val'):
        os.mkdir('./plots/val')
        if not os.path.exists('./plots/val/'+deg_type):
            os.mkdir('./plots/val/'+deg_type)
    plt.savefig('./plots/val/'+deg_type+'/val_avg_performance_with_std_dev.png')
    plt.show()


if __name__ == "__main__":
    ls = os.listdir(directory)
    for folder in ls:
        city_name = folder.split('_')[0]
        if city_name in val:
            if not results_dictionary.__contains__(folder):
                results_dictionary[folder] = dict()
            for deg in os.listdir(os.path.join(directory, folder)):
                deg_name = deg.split('_')[0]
                if deg_name == deg_type:
                    if not results_dictionary[folder].__contains__(deg):
                        results_dictionary[folder][deg] = 0
                    files = os.listdir(os.path.join(directory, folder, deg))
                    for file in files:
                        if len(file.split('.')) < 2:
                            reader = open(os.path.join(directory, folder, deg, file), 'r')
                            lines = reader.readlines()
                            for line in lines:
                                if line.__contains__('Mean IoU'):
                                    line_split = line.split(':')
                                    current_mIoU = float(line_split[1])  # read mean iou value
                                    results_dictionary[folder][deg] = current_mIoU

    results_dictionary_len = len(results_dictionary)
    average_per_city_dict = average_per_city(results_dictionary)
    average_per_split_dict = average_per_split(results_dictionary, results_dictionary_len)
    std_dev = dict()
    std_dev_val_split = dict()
    city_occurrence_count = dict()
    for i in results_dictionary:
        name = i.split('_')[0]
        if name not in city_occurrence_count:
            city_occurrence_count[name] = 0
        city_occurrence_count[name] += 1

        if name not in std_dev:
            std_dev[name] = dict()
        for j in results_dictionary.get(i):
            if j not in std_dev[name]:
                std_dev[name][j] = 0
            if j not in std_dev_val_split:
                std_dev_val_split[j] = 0
            # this code does std_dev of individual city splits
            current_value = results_dictionary.get(i).get(j)
            mean = average_per_city_dict.get(name).get(j)
            squared_difference = (current_value - mean) ** 2
            std_dev[name][j] += squared_difference

            # this code does std_dev of entire evaluation split
            mean = average_per_split_dict.get(j)
            squared_difference = (current_value - mean) ** 2
            std_dev_val_split[j] += squared_difference


    for i in std_dev:
        for j in std_dev.get(i):
            std_dev[i][j] /= city_occurrence_count.get(i)
            std_dev[i][j] = math.sqrt(std_dev[i][j])

    for i in std_dev_val_split:
        std_dev_val_split[i] /= results_dictionary_len
        std_dev_val_split[i] = math.sqrt(std_dev_val_split[i])

    # # only need to plot now - THIS PLOTS AVG PERFORMANCE AND STD_DEV FOR EACH CITY
    for city in average_per_city_dict:
        x_values = []
        y_values = []
        err_values = []
        for i in compression_levels:
            y_values.append(average_per_city_dict.get(city).get(deg_type+'_'+i))
            if (i == '100' and deg_type == 'compress') or (i == '0' and (deg_type == 'blur' or deg_type == 'noise')):
                x_values.append('Normal image')
            else:
                x_values.append(i)
            err_values.append(std_dev.get(city).get(deg_type+'_'+i))
        plot_cities(x_values, y_values, err_values, city)
        # plt.errorbar(x_values, y_values, err_values)
        # plt.show()

    # this plots avg performance of evaluation split and std_dev
    x_values = []
    y_values = []
    err_values = []
    for i in compression_levels:
        y_values.append(average_per_split_dict.get(deg_type+'_'+i))
        if (i == '100' and deg_type == 'compress') or (i == '0' and (deg_type == 'blur' or deg_type == 'noise')):
            x_values.append('Normal image')
        else:
            x_values.append(i)
        err_values.append(std_dev_val_split.get(deg_type+'_'+i))
    plot_val_split(x_values, y_values, err_values)
