import os

import torch
from metrics import StreamSegMetrics
import argparse
from utils import ext_transforms as et
import network
import torch.nn as nn
import utils
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from torch.utils import data
from PIL import Image
from cityscapes_individual import CityscapesIndividual


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, default="./test_results")
    parser.add_argument("--label", type=str, default=None)
    parser.add_argument("--num_classes", type=int, default=19)
    parser.add_argument("--ckpt", default=None, type=str,
                        help="restore from checkpoint")
    available_models = sorted(name for name in network.modeling.__dict__ if name.islower() and \
                              not (name.startswith("__") or name.startswith('_')) and callable(
        network.modeling.__dict__[name])
                              )
    parser.add_argument("--type", type=str, default='blurry')
    parser.add_argument("--gpu_id", type=str, default='0',
                        help="GPU ID")
    parser.add_argument("--crop_val", action='store_true', default=False,
                        help='crop validation (default: False)')
    parser.add_argument("--model", type=str, default='deeplabv3plus_resnet101',
                        choices=available_models, help='model name')
    parser.add_argument("--val_batch_size", type=int, default=2)
    parser.add_argument("--save", action='store_true', default=False)
    parser.add_argument("--stop_at", type=int, default=None)
    return parser


def main():
    opts = get_arguments().parse_args()
    metrics = StreamSegMetrics(opts.num_classes)
    metrics.reset()

    val_transform = et.ExtCompose([
        # et.ExtResize( 512 ),
        et.ExtToTensor(),
        et.ExtNormalize(mean=[0.485, 0.456, 0.406],
                        std=[0.229, 0.224, 0.225]),
    ])

    altered_images = []
    labels_path = []

    typ = opts.type

    init_path = './' + typ
    label_path = './datasets/data/cityscapes/gtFine'
    root_path = './datasets/data/cityscapes'

    for sub_directories in os.listdir(init_path):
        path_1 = init_path + '/' + sub_directories
        split_directories = os.listdir(path_1)
        for split in split_directories:
            if split != "test" and split != "train":
                path_2 = path_1 + '/' + split
                list_city_dir = os.listdir(path_2)
                for city in list_city_dir:
                    path_3 = path_2 + '/' + city
                    images = os.listdir(path_3)
                    for image in images:
                        image_name_split = image.split('_')
                        if len(image_name_split) > 2:  # this check is implemented because jupyter saves checkpoints and they should be ignored
                            altered_images.append(path_3 + '/' + image)
                            label_name = city + '_' + image_name_split[1] + '_' + image_name_split[
                                2] + '_gtFine_labelIds.png'
                            labels_path.append(label_path + '/' + split + '/' + city + '/' + label_name)
                        else:
                            print(f"this is ignored name_split: {image_name_split}")
    index_ckpt = 0
    file = open('index_checkpoint.txt', 'r')  # this will throw an error if the file does not exist
    if file:
        text = file.readline()
        text_split = text.split(':')
        index_ckpt = text_split[len(text_split) - 1]
        print(f"resuming from index checkpoint: {index_ckpt}")

    assert len(altered_images) == len(labels_path)

    os.environ['CUDA_VISIBLE_DEVICES'] = opts.gpu_id
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print("\nDevice: %s" % device)

    model = network.modeling.__dict__[opts.model](num_classes=opts.num_classes, output_stride=16)
    utils.set_bn_momentum(model.backbone, momentum=0.01)
    checkpoint = torch.load(opts.ckpt, map_location=torch.device('cpu'))
    model.load_state_dict(checkpoint["model_state"])
    model = nn.DataParallel(model)
    model.to(device)
    model = model.eval()
    denorm = utils.Denormalize(mean=[0.485, 0.456, 0.406],
                               std=[0.229, 0.224, 0.225])
    for i in range(int(index_ckpt), len(altered_images)):  # do predictions for all images in e.g: ./blurry/blurred_...
        if i % 50 == 0:
            print(f"{i} of {len(altered_images)} is {round((i / len(altered_images)) * 100, 2)}%")
            file = open('index_checkpoint.txt', 'w')  # if this text file does not exist it will throw an error
            file.write('LAST INDEX PROCESSED:' + str(i))
            print(f"saving index checkpoint: {i}")

        image_path = altered_images[i]
        label_path = labels_path[i]
        path_split = image_path.split('/')
        image_name_parts = path_split[len(path_split) - 1]
        city_numbers_type = image_name_parts.split('_')
        assert len(city_numbers_type) == 4
        city = city_numbers_type[0]  # city name
        city_number_1 = city_numbers_type[1]  # first number in image name
        city_number_2 = city_numbers_type[2]  # second number in image name
        split_blur_directory_name = path_split[2].split('_')  # split blurred_images_0
        deg_name = ''
        if path_split[1] == 'compression':
            deg_name = 'compress'
        elif path_split[1] == 'blurry':
            deg_name = 'blur'
        elif path_split[1] == 'noisy':
            deg_name = 'noise'
        strength = split_blur_directory_name[
            len(split_blur_directory_name) - 1]

        val_dst = CityscapesIndividual(root=root_path, image=image_path, target=label_path,
                                       split='val', transform=val_transform)

        batch_size = 1  # or opts.val_batch_size

        val_loader = data.DataLoader(
            val_dst, batch_size=batch_size, shuffle=True, num_workers=2)

        img_id = 0
        with torch.no_grad():
            for j, (images, labels) in enumerate(val_loader):
                images = images.to(device, dtype=torch.float32)
                labels = labels.to(device, dtype=torch.long)

                outputs = model(images)
                preds = outputs.detach().max(dim=1)[1].cpu().numpy()
                targets = labels.cpu().numpy()

                metrics.update(targets, preds)

                save_location = './results_resnet101'

                if not os.path.exists(save_location):
                    os.mkdir(save_location)

                if not os.path.exists(save_location + '/' + city + '_' + city_number_1 + '_' + city_number_2):
                    os.mkdir(save_location + '/' + city + '_' + city_number_1 + '_' + city_number_2)

                if not os.path.exists(
                        save_location + '/' + city + '_' + city_number_1 + '_' + city_number_2 + '/' + deg_name + '_' + strength):
                    os.mkdir(
                        save_location + '/' + city + '_' + city_number_1 + '_' + city_number_2 + '/' + deg_name + '_' + strength)
                save_location = save_location + '/' + city + '_' + city_number_1 + '_' + city_number_2 + '/' + deg_name + '_' + strength

                file = open(save_location + '/' + str(img_id), 'w')
                file.write(metrics.to_str(metrics.get_results()))
                metrics.reset()
                for k in range(len(images)):
                    image = images[k].detach().cpu().numpy()
                    target = targets[k]
                    pred = preds[k]

                    image = (denorm(image) * 255).transpose(1, 2, 0).astype(np.uint8)
                    target = val_loader.dataset.decode_target(target).astype(np.uint8)
                    pred = val_loader.dataset.decode_target(pred).astype(np.uint8)

                    Image.fromarray(image).save(save_location + '/%d_image.png' % img_id)
                    Image.fromarray(target).save(save_location + '/%d_target.png' % img_id)
                    Image.fromarray(pred).save(save_location + '/%d_pred.png' % img_id)

                    plt.imshow(image)
                    plt.axis('off')
                    plt.imshow(pred, alpha=0.7)
                    ax = plt.gca()
                    ax.xaxis.set_major_locator(matplotlib.ticker.NullLocator())
                    ax.yaxis.set_major_locator(matplotlib.ticker.NullLocator())
                    plt.savefig(save_location + '/%d_overlay.png' % img_id, bbox_inches='tight', pad_inches=0)
                    plt.close()
                    img_id += 1


if __name__ == "__main__":
    main()
