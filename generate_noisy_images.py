
import os
import cv2
import numpy as np
from PIL import Image

PATH = './noisy'


def shot_noise(x, severity):
    c = [1000, 750, 500, 250, 100][severity - 1]

    x = np.array(x) / 255.
    return np.clip(np.random.poisson(x * c) / c, 0, 1) * 255



def main():
    initial_path = './datasets/data/cityscapes/leftImg8bit'
    leftImg8bit = os.listdir(initial_path)

    if not os.path.exists(PATH):
        os.mkdir(PATH)
    steps = [x for x in range(6)]
    c = [0, 1000, 750, 500, 250, 100]
    for i in steps:
        print(f"doing strength: {i}")
        path_1 = PATH + "/noisy_images_" + str(c[i])
        if not os.path.exists(path_1):
            os.mkdir(path_1)
        for split in leftImg8bit:
            print(f"doing split:{split}")
            if split != "test" and split != "train":
                path_2 = path_1 + '/' + split
                if not os.path.exists(path_2):
                    os.mkdir(path_2)
                split_directory = os.listdir(initial_path + '/' + split)
                for city in split_directory:
                    print(f"doing city: {city}")
                    path_3 = path_2 + '/' + city
                    if not os.path.exists(path_3):
                        os.mkdir(path_3)
                    city_directory = os.listdir(initial_path + '/' + split + '/' + city)
                    for file in city_directory:
                        path_4 = path_3 + '/' + file
                        if i > 0:
                            try:
                                pth = './datasets/data/cityscapes/leftImg8bit/' + split + '/' + city + '/' + file
                                nparray = shot_noise(Image.open(pth), i)
                                formatted = (nparray * 255 / np.max(nparray)).astype('uint8')
                                Image.fromarray(formatted).save(path_4)
                            except Exception as e:
                                print(f"error: {e}")
                        else:
                            cv2.imwrite(path_4, cv2.imread(
                                './datasets/data/cityscapes/leftImg8bit/' + split + '/' + city + '/' + file))


if __name__ == "__main__":
    main()
