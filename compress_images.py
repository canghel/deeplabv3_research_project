import os
import cv2
from PIL import Image

PATH = './compression'


def compress(img_path, img_name, quality_value, save_to_path):
    img_name_split = img_name.split('.')
    save_split = save_to_path.split('/')
    correct_path = ''
    for elem in range(len(save_split)-1):
        correct_path += save_split[elem] + '/'
    correct_path += img_name_split[0]+'.jpeg'
    Image.open(img_path+img_name).save(correct_path, quality=quality_value)


def main():
    compression_levels = [10, 30, 50, 70, 90, 100]
    initial_path = './datasets/data/cityscapes/leftImg8bit'
    leftImg8bit = os.listdir(initial_path)
    if not os.path.exists(PATH):
        os.mkdir(PATH)
    for i in compression_levels:
        print(f"doing compression: {i}")
        path_1 = PATH + "/compressed_images_" + str(i)
        if not os.path.exists(path_1):
            os.mkdir(path_1)
        for split in leftImg8bit:
            print(f"doing split:{split}")
            if split != "test" and split != "train":
                path_2 = path_1 + '/' + split
                if not os.path.exists(path_2):
                    os.mkdir(path_2)
                split_directory = os.listdir(initial_path + '/' + split)
                for city in split_directory:

                    print(f"doing city: {city}")
                    path_3 = path_2 + '/' + city
                    if not os.path.exists(path_3):
                        os.mkdir(path_3)
                    city_directory = os.listdir(initial_path + '/' + split + '/' + city)
                    for file in city_directory:
                        path_4 = path_3 + '/' + file

                        if i < 100:
                            try:
                                pth = './datasets/data/cityscapes/leftImg8bit/' + split + '/' + city + '/'
                                compress(pth, file, i, path_4)
                            except Exception:
                                print(
                                    "error while reading image ./datasets/data/cityscapes/leftImg8bit/" + split + "/" + city + "/" + file)
                        else:
                            cv2.imwrite(path_4, cv2.imread(
                                './datasets/data/cityscapes/leftImg8bit/' + split + '/' + city + '/' + file))


if __name__ == "__main__":
    main()
